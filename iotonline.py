from threading import Timer
import os
import requests

api_url_base = 'http://alugueap.com/userpweb/api/rasp/iot-online.php'
idiot = 0;
myip = '000.000.000.000';


def find_ip():
    mine = os.popen('ifconfig wlan0 | grep "inet 10" | cut -c 14-25')
    myip = mine.read()
    return myip


def get_id_iot():
    f = open('/home/pi/iot-online/idiot.txt', 'r')

    for line in f:
        line = line.rstrip('\n')
        idiot = line

    return idiot


def call_API():
    #print('Meu ip: ' + find_ip() + ' - ID: ' + get_id_iot())
    response = requests.get(api_url_base + "?idiot=" + get_id_iot() + "&ipiot=" + find_ip())
    t = Timer(30.0, chamarAPI)
    t.start()


chamarAPI()
